package com.nexsoft.project1.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.LinkedHashMap;

import org.springframework.stereotype.Service;


@Service
public class PriceService {

    private LinkedHashMap<LocalDate, Double> datePrice = new LinkedHashMap<LocalDate, Double>();

    private Double normalPrice = 600.0;

    public LinkedHashMap<LocalDate, Double> getPrice(){
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = LocalDate.now().plusYears(1);

        double price;

        for(LocalDate date = startDate; date.isBefore(endDate) ; date = date.plusDays(1)){
            price = normalPrice;

            int diffDay = date.getDayOfYear() - startDate.getDayOfYear();

            int diffMonth = Period.between(startDate, date).getMonths();

            int year = date.getYear();

            LocalDate startSummer = LocalDate.parse( year +"-06-28");  

            LocalDate endSummer = LocalDate.parse( year +"-09-22");  

            LocalDate startWinter = LocalDate.parse(year + "-12-21");

            LocalDate endWinter = LocalDate.parse((year+1) + "-03-20");

            if( diffDay== 0 || diffDay ==1 ){
                price = 1.2 * normalPrice;
            }

            if( diffDay <=5 && diffDay >=2){
                price = 1.1 * normalPrice;
            }

            if(diffMonth >= 6 && diffMonth <=9 ){
                price = 0.975 * normalPrice;
            }

            if(diffMonth >=10 && diffMonth <=12){
                price = 0.85 * normalPrice;
            }

            if(date.isAfter(startSummer) && date.isBefore(endSummer)){
                price = price + 0.2 * normalPrice;
            }

            if(date.isAfter(startWinter) && date.isBefore(endWinter)){
                price = price + 0.15 * normalPrice;
            }

            datePrice.put(date, price);
        }
        return datePrice;

    }

    public LinkedHashMap<LocalDate, Double> getPricePerMonth(int month){
        LinkedHashMap<LocalDate, Double> datePrice  = getPrice();

        LinkedHashMap<LocalDate, Double> datePricePerMonth = new LinkedHashMap<LocalDate, Double>();

        for (LocalDate i : datePrice.keySet()) {
            if(i.getMonthValue() != month ){
                continue;
            }

            datePricePerMonth.put(i, datePrice.get(i));
        }
        return datePricePerMonth;

    }

    
}
