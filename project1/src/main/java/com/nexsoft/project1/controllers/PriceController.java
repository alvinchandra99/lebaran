package com.nexsoft.project1.controllers;

import com.nexsoft.project1.service.PriceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.LinkedHashMap;
import java.time.LocalDate;

@Controller
@RequestMapping("/")
public class PriceController {

    @Autowired
    PriceService priceService;

    @GetMapping("/showprice")
    public String showPrice(Model model){

        LinkedHashMap<LocalDate, Double> datePrice = priceService.getPrice();
        
        model.addAttribute("datePrice", datePrice);   
        return "showprice";
    }

    @GetMapping("/showprice/{id}")
    public String showPricee(Model model, @PathVariable int id){

        LinkedHashMap<LocalDate, Double> datePrice = priceService.getPricePerMonth(id);

        model.addAttribute("datePrice", datePrice);   
        model.addAttribute("id", id);  
        return "showpricee";
    }

 
}
