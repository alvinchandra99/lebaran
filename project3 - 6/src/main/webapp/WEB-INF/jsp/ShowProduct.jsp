<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglibprefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0"
      crossorigin="anonymous"
    />

    <title>Detail Product</title>
  </head>
  <body>
    <div class="container mt-5">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Image</th>
            <th scope="col">Artnumber</th>
            <th scope="col">Brand</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Stock</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach items="${product}" var="p">
            <tr>
           
              <td><img src=" ${p.getImgUrl()}" class="rounded float-start" alt="Gambar Produk" width="150px" height="150px"></td>
              <td>${p.getArtnumber()}</td>
              <td>${p.getBrand().getName()}</td>
              <td>${p.getName()}</td>
              <td>${p.getDescription()}</td>
              <td>${p.getStock()}</td>
              <td>
           
                  <a href="/detail/${p.getId()}" type="button" class="btn btn-success">Details</a>
                    <button data-bs-toggle="modal" data-bs-target="#exampleModal" type="submit" class="btn btn-danger">Delete</button>
        
               <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Confirm Message</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                Apakah yakin ingin menghapus product?
                </div>
                <div class="modal-footer">
            
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <form action="delete/${p.getId()}" method="POST">
                  <button type="submit" class="btn btn-primary">Delete</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
              </td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
      <a href="/insertproduct" type="button" class="btn btn-primary">Insert Product</a>
    </div>


    



    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
