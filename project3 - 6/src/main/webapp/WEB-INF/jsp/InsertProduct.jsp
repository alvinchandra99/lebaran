<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglibprefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0"
      crossorigin="anonymous"
    />

    <title>Insert Product Page</title>
  </head>
  <body>
    <div class="container mt-5">
      <div class="mb-4">
        <h2>Insert Products</h2>
      </div>

      <form action="saveproduct" method="POST">
        <div class="mb-3">
          <label class="form-label">Product Name</label>
          <input
            name="name"
            class="form-control"
            type="text"
            placeholder="Masukan nama produk"
            aria-label="default input example"
          />
        </div>
        <div class="mb-3">
          <label class="form-label">Artnumber</label>
          <input
            name="artnumber"
            class="form-control"
            type="text"
            placeholder="artikel number"
            aria-label="default input example"
          />
        </div>

        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label"
            >Description</label
          >
          <textarea
            name="description"
            class="form-control"
            id="exampleFormControlTextarea1"
            rows="3"
          ></textarea>
        </div>

        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label"
            >Stock</label
          >
          <input
            name="stock"
            class="form-control"
            type="text"
            placeholder="Default input"
            aria-label="default input example"
          />
        </div>

        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label"
            >Image Url</label
          >
          <input
            name="imgUrl"
            class="form-control"
            type="text"
            placeholder="Default input"
            aria-label="default input example"
          />
        </div>

        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label"
            >Brand</label
          >
          <select
            name="brandId"
            class="form-select"
            aria-label="Default select example"
          >
            <option selected>Pilih Brand</option>
            <option value="1">Xiaomi</option>
            <option value="2">Samsung</option>
          </select>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
