package com.nexsoft.project3.models.services;

import javax.transaction.TransactionScoped;

import com.nexsoft.project3.models.entities.Brand;
import com.nexsoft.project3.models.repository.BrandRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class BrandService {

    @Autowired
    BrandRepository brandRepository;

    public Brand save(Brand brand){
        return brandRepository.save(brand);
    }

    
}
