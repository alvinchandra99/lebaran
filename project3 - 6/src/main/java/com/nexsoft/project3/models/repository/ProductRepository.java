package com.nexsoft.project3.models.repository;

import java.util.List;

import com.nexsoft.project3.models.entities.Product;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

    @Query(value = "SELECT * from product LEFT JOIN brand on product.brand_id = brand.id", nativeQuery = true)
    public List<Product> findAllProductJoinBrand();
    
}
