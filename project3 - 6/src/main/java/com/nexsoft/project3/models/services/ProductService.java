package com.nexsoft.project3.models.services;

import java.util.List;


import javax.transaction.TransactionScoped;

import com.nexsoft.project3.models.entities.Product;
import com.nexsoft.project3.models.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Product save(Product product)
    {
        return productRepository.save(product);
    }

    public Product findProductById(int id){
        return productRepository.findById(id).get();
    }


    public List<Product> findAllProductJoinBrand(){
        return productRepository.findAllProductJoinBrand();
    }

    public void deleteProduct(int id){
        productRepository.deleteById(id);
    }
    
}
