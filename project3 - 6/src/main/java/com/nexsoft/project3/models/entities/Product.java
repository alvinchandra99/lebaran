package com.nexsoft.project3.models.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "product")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String artnumber;

    @Column(length = 500 )
    private String description;

    private String imgUrl;

    private int stock;

    @Column(name = "brand_id")
    private int brandId;

    @ManyToOne
    @JoinColumn(name="brand_id", insertable = false, updatable = false)
    private Brand brand;


    public int getBrandId() {
        return brandId;
    }


    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }


    public Brand getBrand() {
        return brand;
    }


    public void setBrand(Brand brand) {
        this.brand = brand;
    }


    public Product(int id, String name, String artnumber, String description, String imgUrl, int stock, int brandId,
            Brand brand) {
        this.id = id;
        this.name = name;
        this.artnumber = artnumber;
        this.description = description;
        this.imgUrl = imgUrl;
        this.stock = stock;
    
    }


    public Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtnumber() {
        return artnumber;
    }

    public void setArtnumber(String artnumber) {
        this.artnumber = artnumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }


    

    

    
    
}
