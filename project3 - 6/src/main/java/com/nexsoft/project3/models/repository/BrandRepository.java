package com.nexsoft.project3.models.repository;


import com.nexsoft.project3.models.entities.Brand;

import org.springframework.data.repository.CrudRepository;

public interface BrandRepository extends CrudRepository<Brand, Integer> {


    
}
