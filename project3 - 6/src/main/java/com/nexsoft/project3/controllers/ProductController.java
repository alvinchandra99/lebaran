package com.nexsoft.project3.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import com.nexsoft.project3.models.entities.Product;
import com.nexsoft.project3.models.services.ProductService;

@RestController
@RequestMapping("/")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping
    public String welcome(){
        return "welcome";
    }

    @GetMapping("/insertproduct")
    public ModelAndView insertProduct(){
        ModelAndView mv= new ModelAndView("InsertProduct");
        return mv;
    }

    @PostMapping("/saveproduct")
    public String saveProduct(Product product){
        productService.save(product);

        

        return "Berhasil Save Product";
    }

    @GetMapping("/product")
    public ModelAndView showProduct(){
        ModelAndView mv= new ModelAndView("ShowProduct");
        mv.addObject("product", productService.findAllProductJoinBrand());
        // List<Product> produk = productService.findAllProductJoinBrand();
       
        return mv;
    }

    @GetMapping("/detail/{id}")
    public ModelAndView showDetailProduct(@PathVariable int id){
        ModelAndView mv= new ModelAndView("DetailProduct");
        mv.addObject("detail",productService.findProductById(id));

        return mv;
    }
    
    @PostMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id){
        productService.deleteProduct(id);


        return "Berhasil Menghapus Product";

    }

    

    
    
}
